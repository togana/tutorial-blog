import React, { Component } from 'react';

export default class BoardForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: props.board.title || '',
      message: props.board.message || '',
    };
  }

  setTitle(text) {
    this.setState({ ...this.state, title: text.target.value });
  }

  setMessage(text) {
    this.setState({ ...this.state, message: text.target.value });
  }

  render() {
    return (
      <div>
        <form action="/boards" acceptCharset="UTF-8" method="post" >
          <input name="utf8" type="hidden" value="✓" />
          <input type="hidden" name="authenticity_token" value={this.props.token} />
          <div className="field">
            <label htmlFor="board_title">Title</label>
            <input id="board_title" type="text" name="board[title]" value={this.state.title} onChange={(text) => this.setTitle(text)} />
          </div>
          <div className="field">
            <label htmlFor="board_message">Message</label>
            <input id="board_message" type="text" name="board[message]" value={this.state.message} onChange={(text) => this.setMessage(text)} />
          </div>
          <div className="actions">
            <input type="submit" name="commit" value={this.props.submitText} data-disable-with={this.props.submitText} />
          </div>
        </form>
      </div>
    );
  }
}
