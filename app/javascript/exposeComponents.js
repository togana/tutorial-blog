import { renderReact } from 'hypernova-react';
import _Board from './packs/board';
import _BoardForm from './packs/board/form';

export const Board = renderReact('Board', _Board);
export const BoardForm = renderReact('BoardForm', _BoardForm);
