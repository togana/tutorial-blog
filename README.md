tutorial-blog
====

# 準備

```
$ bundle install --path vendor/bundle -j4
$ yarn
$ bundle exec rails db:create
$ bundle exec rails db:migrate
```

# 実行

```
$ bundle exec foreman start
```
