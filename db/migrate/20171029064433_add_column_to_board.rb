class AddColumnToBoard < ActiveRecord::Migration[5.1]
  def change
    # 下記でやりたかったけどafterオプションはMySQLでしか動かない
    # add_column :boards, :message, :string, after: :title
    add_column :boards, :message, :string
  end
end
